#ifndef SUBCHAPTERWINDOWS_H
#define SUBCHAPTERWINDOWS_H

#include <QHBoxLayout>
#include <QWidget>
#include <QVBoxLayout>

/* Classe de gestion de l'affichage des sous-chapitre pour un chapitre donné */

class SubChapterWindows
{
    public:
        // Constructeur par défaut de la classe
        SubChapterWindows();

        // Eléments de gestion de l'affichage pour chaque fenêtre
        QWidget chaptWindows;
        QVBoxLayout layout;
        QList<QHBoxLayout*> boxLayout;
};

#endif // SUBCHAPTERWINDOWS_H
