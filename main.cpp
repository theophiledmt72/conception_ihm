#include "IHMWidget.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    IHMWidget ihm;
    ihm.show();
    return a.exec();

}
