#ifndef IHMWIDGET_H
#define IHMWIDGET_H

#include <QString>
#include <QMainWindow>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QBoxLayout>
#include <QFormLayout>
#include <QPixmap>
#include <QImage>
#include <QGroupBox>
#include <QTextStream>
#include <QSignalMapper>
#include <QtDebug>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

#include "chapterWindows.h"
#include "subchapterWindows.h"
#include "parser.h"


class IHMWidget : public QWidget
{
    Q_OBJECT

public:
    IHMWidget(QWidget *parent = nullptr);
    ~IHMWidget();


private:

    /* Attributs permettant de gérer l'interaction avec l'utilisateur et l'affichage des sections du fichier DES */
    bool file_DES_load = false;
    int nbChapter;
    Book qtcours;
    ChapterWindows *chaptWindows = new ChapterWindows;
    QList<SubChapterWindows*> subchapterWindows;
    QSignalMapper *signal = new QSignalMapper;
    QPushButton *chooseDESButton = new QPushButton("Charger DES");

    QLabel *imageLabel;
    QPushButton *chooseImageButton = new QPushButton("Choisir une image");
    QLineEdit *imageWidth = new QLineEdit;
    QLineEdit *imageHeight = new QLineEdit;
    QPushButton *applyButton = new QPushButton("Appliquer");
    QLabel *LED1 = new QLabel;
    QLabel *LED2 = new QLabel;
    QLabel *LED3 = new QLabel;
    QPushButton *button1 = new QPushButton("ON");
    QPushButton *button2 = new QPushButton("ON");
    QPushButton *button3 = new QPushButton("ON");

    QGroupBox* createImageBox();    // method to create the GroupBox with the image label
    QGroupBox* createLEDBox();      // method to create the GroupBox with the LEDs and the switch buttons



private slots:
    void chooseImage();
    void applyChanges();
    void toggleLED1(bool);
    void toggleLED2(bool);
    void toggleLED3(bool);

    /* Méthodes utilisées pour l'extraction du chemin du fichier DES, l'ouverture de la fenêtre des chapitres,
    puis des sous-chapitres selon l'utilisation du clic */
    void loadDESFile();
    void OpenWindowsChapter();
    void OpenWindowsSubChapter(const QString &text);
};

#endif // IHMWIDGET_H
