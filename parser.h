#include <string>
#include <vector>
#include <QString>

#ifndef PARSER_H
#define PARSER_H

/* Structures nécessaires au parsing du fichier DES */
/* On utilise une structure pour définir l'objet qtcours contenant une liste de chapitre (elle même qui contient une liste de sous-chapitre) */
struct SubChapter
{
    int chap_number;
    std::string name;
    int offset_sub_chap;
};

struct Chapter
{
    int numerous;
    std::string name;
    int page_subchapter_number;
    int page_number;
    std::vector <SubChapter> sub_chap;
};

struct Book
{
    std::vector <Chapter> chapt;
    int nb_chapt;
};

class parser
{
    public:

        static Book generate(QString *DESPath);     // Méthode de parsing du fichier DES

    protected:

    private:
};

#endif // PARSER_H
