#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QGroupBox *imageBox = new QGroupBox;
    imageBox->setTitle("Image");
    imageLabel = new QLabel(this);
    imageLabel->setFixedSize(512, 512);
    imageLabel->setScaledContents(true);
    QHBoxLayout *imageLayout = new QHBoxLayout;
    imageLayout->addWidget(imageLabel);
    imageBox->setLayout(imageLayout);

    QGroupBox *ledBox = new QGroupBox;
    QHBoxLayout *LEDlayout = new QHBoxLayout;
    LEDlayout->addWidget(LED1);
    LED1->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED1->setFixedSize(30, 30);
    LED1->setScaledContents(true);
    LEDlayout->addWidget(LED2);
    LED2->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED2->setFixedSize(30, 30);
    LED2->setScaledContents(true);
    LEDlayout->addWidget(LED3);
    LED3->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED3->setFixedSize(30, 30);
    LED3->setScaledContents(true);
    ledBox->setLayout(LEDlayout);
    ledBox->setTitle("LEDs");
    ledBox->setFixedHeight(100);

    QVBoxLayout *topLayout = new QVBoxLayout;
    topLayout->addWidget(ledBox);
    topLayout->addWidget(imageBox);
    topLayout->addWidget(chooseImageButton);
    connect(chooseImageButton, SIGNAL(clicked()), this, SLOT(chooseImage()));

    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow("Largeur de l'image :", imageWidth);
    formLayout->addRow("Hauteur de l'image :", imageHeight);
    formLayout->addWidget(applyButton);
    topLayout->addLayout(formLayout);
    connect(applyButton, SIGNAL(clicked()), this, SLOT(applyChanges()));

    this->setLayout(topLayout);
}

MainWindow::~MainWindow()
{
}

void MainWindow::chooseImage()
{
    QString imagePath = QFileDialog::getOpenFileName(this,
                                                      tr("Sélectionnez une image"),
                                                     "",
                                                      "Images (*.png *.bmp *.jpg)");

    if (imagePath == nullptr)
        return;
    else
    {
        QPixmap *pix = new QPixmap(imagePath);
        imageLabel->setPixmap(*pix);
    }
}

void MainWindow::applyChanges()
{
    int width = 0;
    int height = 0;
    width = imageWidth->text().toInt();
    height = imageHeight->text().toInt();
    imageLabel->setFixedSize(width, height);
}
