#ifndef CHAPTERWINDOWS_H
#define CHAPTERWINDOWS_H

#include <QPushButton>
#include <QWidget>
#include <QVBoxLayout>

/* Classe de gestion de l'affichage des chapitres après chargement du fichier DES */

class ChapterWindows
{
    public:
        // Constructeur par défaut de la classe
        ChapterWindows();

        // Eléments de gestion de l'affichage pour chaque fenêtre
        QPushButton **chaptButton = new QPushButton*;
        QWidget *chaptWindows = new QWidget;
        QVBoxLayout *chaptLayout = new QVBoxLayout;
};

#endif // CHAPTERWINDOWS_H
