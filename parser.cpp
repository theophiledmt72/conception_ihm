#include "parser.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

Book parser::generate(QString *DESPath)
{
    /* Initialisation */
    bool chapter = false;
    bool subchapter = false;
    std::string line;
    std::ifstream input;
    std::istringstream isstream;
    std::string filename = DESPath->toLocal8Bit().constData();
    bool check_chapter = false;
    bool check_sub_chapter = false;
    std::string word, clean_word;
    int nb_chapt, nb_sub_chapt, i;
    std::string CHAPT_TAG = "<chapitre>";
    std::string SUB_CHAPT_TAG = "<subchapt>";

    /* Instanciation de l'objet Book */
    Book qtcours;

    /* Ouverture du fichier d'instance */
    input.open(filename);

    /* On vérifie si le fichier s'est correctement ouvert */
    if(input.is_open())
    {
        Chapter chapt;

        /* On lit ligne par ligne */
        while(input)
        {
            std::getline(input, line);

            /* Variables permettant de délimiter le début et la fin de ligne */
            auto const begin_line { std::begin(line) };
            auto const end_line { std::end(line) };

            /* On vérifie si il s'agit d'une ligne pour un chapitre */
            check_chapter = (std::search(begin_line, end_line, std::begin(CHAPT_TAG), std::end(CHAPT_TAG)) != end_line);
            if (check_chapter)
            {
                chapter = true;
            }

            /* On vérifie si il s'agit d'une ligne pour un sous-chapitre */
            check_sub_chapter = (std::search(begin_line, end_line, std::begin(SUB_CHAPT_TAG), std::end(SUB_CHAPT_TAG)) != end_line);
            if (check_sub_chapter)
            {
                subchapter = true;
            }


            if (chapter == true && subchapter == false)
            {
                isstream.str(line);

                /* On place le flag à la poubelle */
                getline(isstream, word, ' ');

                /* On extrait le numéro de chapitre */
                getline(isstream, word, ';');
                nb_chapt = atoi(word.c_str());
                chapt.numerous = nb_chapt;

                /* On extrait le nom de chapitre */
                getline(isstream, word, ';');
                word.erase(std::remove_if(begin(word), end(word), isspace), end(word));
                chapt.name = word;

                /* On extrait le numéro de page de chapitre */
                getline(isstream, word, ';');
                nb_chapt = atoi(word.c_str());
                chapt.page_number = nb_chapt;

                /* On extrait le nombre de sous-chapitre */
                getline(isstream, word, ';');
                nb_chapt = atoi(word.c_str());
                chapt.page_subchapter_number = nb_chapt;
                nb_sub_chapt = nb_chapt;

                chapter = false;

                i = 0;
            }

            if (chapter == false && subchapter == true)
            {
                isstream.str(line);

                SubChapter sub_chapter;

                /* On place le flag à la poubelle */
                getline(isstream, word, ' ');

                /* On extrait le numéro du sous-chapitre */
                getline(isstream, word, ';');
                nb_chapt = atoi(word.c_str());
                sub_chapter.chap_number = nb_chapt;

                /* On extrait le nom du sous-chapitre */
                getline(isstream, word, ';');
                word.erase(std::remove_if(begin(word), end(word), isspace), end(word));
                sub_chapter.name = word;

                /* On extrait l'offset du sous-chapitre */
                getline(isstream, word, ';');
                nb_chapt = atoi(word.c_str());
                sub_chapter.offset_sub_chap = nb_chapt;

                /* On pousse le nouvel objet dans la liste des sous-chapitres du chapitre traité */
                chapt.sub_chap.push_back(sub_chapter);
                subchapter = false;

                i++;

                if(i == nb_sub_chapt)
                {
                    /* Une fois que tous les sous-chapitres ont été récupérés, on pousse le chapitre (qui contient une liste de sous-chapitre) */
                    qtcours.chapt.push_back(chapt);
                    chapt.sub_chap.clear();
                }
            }
        }

        // Fermeture du fichier
        input.close();
    }

    return qtcours;
}
