#include "IHMWidget.h"

IHMWidget::IHMWidget(QWidget *parent)
    : QWidget(parent)
{
    QHBoxLayout *topLayout = new QHBoxLayout;

    topLayout->addWidget(createImageBox());

    /* Création du layout vertical qui contient la LEDBox
     *  et les boutons/lineedit pour paramétrer l'image affichée */
    QVBoxLayout *vLayout = new QVBoxLayout;
    vLayout->addWidget(createLEDBox());
    vLayout->addWidget(chooseImageButton);
    connect(chooseImageButton, SIGNAL(clicked()), this, SLOT(chooseImage()));
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow("Largeur de l'image :", imageWidth);
    formLayout->addRow("Hauteur de l'image :", imageHeight);
    formLayout->addWidget(applyButton);
    vLayout->addLayout(formLayout);
    connect(applyButton, SIGNAL(clicked()), this, SLOT(applyChanges()));
    topLayout->addLayout(vLayout);

    this->setLayout(topLayout);
}

IHMWidget::~IHMWidget()
{
}

QGroupBox* IHMWidget::createImageBox()
{
    QGroupBox *imageBox = new QGroupBox;
    imageBox->setTitle("Image");
    imageLabel = new QLabel(this);
    imageLabel->setFixedSize(512, 512);
    imageLabel->setScaledContents(true);
    QHBoxLayout *imageLayout = new QHBoxLayout;
    imageLayout->addWidget(imageLabel);
    imageBox->setLayout(imageLayout);

    return imageBox;
}

QGroupBox* IHMWidget::createLEDBox()
{
    QGroupBox *ledBox = new QGroupBox;
    QVBoxLayout *ledBoxLayout = new QVBoxLayout;

    QHBoxLayout *LEDlayout = new QHBoxLayout;
    LEDlayout->addWidget(LED1);
    LED1->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED1->setFixedSize(30, 30);
    LED1->setScaledContents(true);
    LEDlayout->addWidget(LED2);
    LED2->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED2->setFixedSize(30, 30);
    LED2->setScaledContents(true);
    LEDlayout->addWidget(LED3);
    LED3->setPixmap(QPixmap(":/images/LED_grey.png"));
    LED3->setFixedSize(30, 30);
    LED3->setScaledContents(true);
    ledBox->setTitle("LEDs");
    ledBox->setFixedHeight(140);
    ledBoxLayout->addLayout(LEDlayout);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(button1);
    button1->setCheckable(true);
    connect(button1, SIGNAL(toggled(bool)), this, SLOT(toggleLED1(bool)));
    buttonLayout->addWidget(button2);
    button2->setCheckable(true);
    connect(button2, SIGNAL(toggled(bool)), this, SLOT(toggleLED2(bool)));
    buttonLayout->addWidget(button3);
    button3->setCheckable(true);
    connect(button3, SIGNAL(toggled(bool)), this, SLOT(toggleLED3(bool)));
    ledBoxLayout->addLayout(buttonLayout);
    ledBox->setLayout(ledBoxLayout);

    /* Ajout du bouton permettant de choisir le fichier DES */
    ledBoxLayout->addWidget(chooseDESButton);

    /* Lancement de la fonction de chargement du fichier DES suite à la détection du clic sur le bouton */
    connect(chooseDESButton, SIGNAL(clicked()),this, SLOT(loadDESFile()));

    return ledBox;
}

/* slot qui permet de sélectionner une image à afficher */
void IHMWidget::chooseImage()
{
    QString imagePath = QFileDialog::getOpenFileName(this,
                                                      tr("Sélectionnez une image"),
                                                     "",
                                                      "Images (*.png *.bmp *.jpg)");

    if (imagePath == nullptr)
        return;
    else
    {
        QPixmap *pix = new QPixmap(imagePath);
        imageLabel->setPixmap(*pix);
    }
}


/* slot qui permet d'appliquer les changements de taille de l'image */
void IHMWidget::applyChanges()
{
    int width = 0;
    int height = 0;
    width = imageWidth->text().toInt();
    height = imageHeight->text().toInt();
    imageLabel->setFixedSize(width, height);
}

void IHMWidget::toggleLED1(bool checked)
{
    if (checked)
    {
        LED1->setPixmap(QPixmap(":/images/LED_green.png"));
        button1->setText("OFF");
    }
    else
    {
        LED1->setPixmap(QPixmap(":/images/LED_grey.png"));
        button1->setText("ON");
    }
}

void IHMWidget::toggleLED2(bool checked)
{
    if (checked)
    {
        LED2->setPixmap(QPixmap(":/images/LED_green.png"));
        button2->setText("OFF");
    }
    else
    {
        LED2->setPixmap(QPixmap(":/images/LED_grey.png"));
        button2->setText("ON");
    }
}

void IHMWidget::toggleLED3(bool checked)
{
    if (checked)
    {
        LED3->setPixmap(QPixmap(":/images/LED_green.png"));
        button3->setText("OFF");
    }
    else
    {
        LED3->setPixmap(QPixmap(":/images/LED_grey.png"));
        button3->setText("ON");
    }
}


/* Méthode utilisée pour sélectionner un fichier DES à l'aide d'un explorateur de fichier */
void IHMWidget::loadDESFile()
{
    QString DESPath = QFileDialog::getOpenFileName(this,
                                                      tr("Sélectionnez un DES"),
                                                     "",
                                                      "DES (*.des)");

    if (DESPath == nullptr)
        return;
    else
    {
        /* On déclare un objet de type Book qui vient récupérer l'objet contenant les éléments parsés
        en retour de la fonction generate */
        qtcours = parser::generate(&DESPath);

        /* Ouverture de la fenêtre contenant les chapitres */
        OpenWindowsChapter();
    }
}


/* Méthode permettant d'ouvrir la fenêtre des chapitres sous forme de boutons */
void IHMWidget::OpenWindowsChapter()
{
    /* Initialisation des variables */
    int nbButton = 0;

    /* Si le fichier DES a déjà été chargé, on détruit tous les éléments (fenêtre, boutons, layout) */
    if(file_DES_load)
    {
        for(nbButton=0; nbButton<nbChapter; nbButton++)
        {
                chaptWindows->chaptButton[nbButton]->disconnect(chaptWindows->chaptButton[nbButton], SIGNAL(clicked()), signal, SLOT(map()));
                chaptWindows->chaptButton[nbButton]->destroyed(NULL);
                signal->removeMappings(chaptWindows->chaptButton[nbButton]);
        }
        disconnect(signal, SIGNAL(mapped(const QString &)), this, SLOT(OpenWindowsSubChapter(const QString &)));

        chaptWindows->chaptWindows->destroyed();
        chaptWindows->chaptWindows = new QWidget;

        chaptWindows->chaptLayout->destroyed();
        chaptWindows->chaptLayout = new QVBoxLayout;

        nbButton=0;
        nbChapter=0;
    }

    /* On boucle sur chaque chapitre en créant un bouton avec pour label le nom du chapitre
    Puis, on applique la connexion dès la détection d'un clic */
    for(Chapter &c : qtcours.chapt)
    {
        QString chapter_name = QString::fromStdString(c.name);
        chaptWindows->chaptButton[nbButton] = new QPushButton(chapter_name);
        connect(chaptWindows->chaptButton[nbButton], SIGNAL(clicked()), signal, SLOT(map()));
        signal->setMapping(chaptWindows->chaptButton[nbButton], chapter_name);
        chaptWindows->chaptLayout->addWidget(chaptWindows->chaptButton[nbButton]);

        /* On augmente le nombre de boutons, de chapitre traités */
        nbButton++;
        nbChapter++;
    }
    connect(signal, SIGNAL(mapped(const QString &)), this, SLOT(OpenWindowsSubChapter(const QString &)));

    /* Mise en forme de la fenêtre d'affichage */
    chaptWindows->chaptWindows->setLayout(chaptWindows->chaptLayout);
    chaptWindows->chaptWindows->setMinimumWidth(450);
    chaptWindows->chaptWindows->setWindowTitle("/qtcourse.des");
    chaptWindows->chaptWindows->showNormal();

    /* On indique que le fichier DES a déjà été chargé */
    file_DES_load=true;
}

/* Méthode permettant d'ouvrir la fenêtre des sous-chapitres pour un chapitre donné */
void IHMWidget::OpenWindowsSubChapter(const QString &button_name)
{
    SubChapterWindows *subchapterwindows = new SubChapterWindows;

    /* Variable globale utilisée pour afficher le label de la fenêtre */
    QString chapt_name;

    for(Chapter &c : qtcours.chapt)
    {
        /* Conversion d'un std::string provenant de la structure en un QString */
        QString chapter_name = QString::fromStdString(c.name);

        /* On vérifie que le label du bouton sur lequel l'utilisateur vient de cliquer correspond à un nom dans la liste */
        if(chapter_name.compare(button_name) == 0)
        {
            /* On récupère dans une variable globale le nom du chapitre pour l'afficher dans la fenêtre de sous-chapitre */
            chapt_name = chapter_name;

            /* Pour chaque sous-chapitre */
            for(SubChapter &sc : c.sub_chap)
            {
                /* Calcul du numéro de page du sous-chapitre */
                int subchapter_page_number = sc.offset_sub_chap + c.page_number;

                /* Conversion d'un std::string en QString, d'un int en QString */
                QString subchapter_name = QString::fromStdString(sc.name);
                QString subchapter_number = QString::number(subchapter_page_number);

                /* Ajout des sous-sections à la page */
                QHBoxLayout *subchaptLayout = new QHBoxLayout;

                QLabel *name = new QLabel(subchapter_name);
                subchaptLayout->addWidget(name);

                QLabel *number = new QLabel(subchapter_number);
                subchaptLayout->addWidget(number);

                subchapterwindows->boxLayout.append(subchaptLayout);
                subchapterwindows->layout.addLayout(subchaptLayout);
            }
            break;
        }
    }

    /* Mise en forme de la fenêtre avant son affichage */
    subchapterwindows->chaptWindows.setLayout(&subchapterwindows->layout);
    subchapterwindows->chaptWindows.setMinimumWidth(450);
    subchapterwindows->chaptWindows.setWindowTitle(chapt_name);
    subchapterWindows.append(subchapterwindows);
    subchapterwindows->chaptWindows.showNormal();
}


